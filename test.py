from __future__ import absolute_import, division, print_function

import os
import utils
import models
import argparse
import numpy as np
import tensorflow as tf
import image_utils as im

from glob import glob


""" param """
parser = argparse.ArgumentParser(description='')
parser.add_argument('--dataset', dest='dataset', default='horse2zebra', help='which dataset to use')
parser.add_argument('--crop_size', dest='crop_size', type=int, default=256, help='then crop to this size')
#parser.add_argument('--test_dir', dest='test_dir', default='./test', help='test sample are saved here')
parser.add_argument('--test_dir', dest='test_dir', default='', help='test sample are saved here')
parser.add_argument('--input_nc', dest='input_nc', type=int, default=3, help='# of input image channels')
parser.add_argument('--output_nc', dest='output_nc', type=int, default=3, help='# of output image channels')
parser.add_argument('--checkpoint_dir', dest='checkpoint_dir', default='./checkpoint', help='models are saved here')

args = parser.parse_args()

dataset = args.dataset
crop_size = args.crop_size
input_c_dim = args.input_nc
output_c_dim = args.output_nc
checkpoint_dir = args.checkpoint_dir
test_dir= args.test_dir

#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

""" run """
with tf.Session() as sess:
    a_real = tf.placeholder(tf.float32, shape=[None, crop_size, crop_size, input_c_dim])
    b_real = tf.placeholder(tf.float32, shape=[None, crop_size, crop_size, input_c_dim])

    a_real_fir = a_real[:,:,:,:6]
    a_real_sec = a_real[:,:,:,3:9]
    
    b_real_fir = b_real[:,:,:,:6]
    b_real_sec = b_real[:,:,:,3:9]
    

    a2b_fir = models.generator(a_real_fir, 'a2b')
    a2b_sec = models.generator(a_real_sec, 'a2b', reuse=True)
    
    b2a_fir = models.generator(b_real_fir, 'b2a')
    b2a_sec = models.generator(b_real_sec, 'b2a', reuse=True)
    
    b2a2b_fir = models.generator(b2a_fir, 'a2b', reuse=True)
    b2a2b_sec = models.generator(b2a_sec, 'a2b', reuse=True)
    
    a2b2a_fir = models.generator(a2b_fir, 'b2a', reuse=True)
    a2b2a_sec = models.generator(a2b_sec, 'b2a', reuse=True)

    # retore
    saver = tf.train.Saver()
    ckpt_path = utils.load_checkpoint(checkpoint_dir + '/' + dataset, sess, saver)
    if ckpt_path is None:
        raise Exception('No checkpoint!')
    else:
        print('Copy variables from % s' % ckpt_path)

    # test
    sample_filesA = glob('./datasets/' + dataset + '/testA/*.nrrd')
    sample_filesB = glob('./datasets/' + dataset + '/testB/*.nrrd')

    a_save_dir = test_dir + '/' + dataset + '/testA/'
    b_save_dir = test_dir + '/' + dataset + '/testB/'

    utils.mkdir([a_save_dir])
    utils.mkdir([b_save_dir])

    for sample_file in sample_filesA:
        sample_image = [im.load_test_data_nrrd(sample_file, crop_size)]
        sample_image = np.array(sample_image).astype(np.float32)
        image_path = os.path.join(a_save_dir,
                                      '{0}_{1}'.format('AtoB', os.path.basename(sample_file)))
       
        
        [a2b_fir_opt, a2b_sec_opt, a2b2a_fir_opt, a2b2a_sec_opt] \
        = sess.run([a2b_fir, a2b_sec, a2b2a_fir, a2b2a_sec], feed_dict={a_real: sample_image})
               
        #copy 6 dim images back to 9 dim images 
        a2b_opt = np.zeros(shape=(1,256,256,9))
        a2b_opt[:,:,:,:6] = a2b_fir_opt
        a2b_opt[:,:,:,6:9] = a2b_sec_opt[:,:,:,3:6]
        
        a2b2a_opt = np.zeros(shape=(1,256,256,9))
        a2b2a_opt[:,:,:,:6] = a2b2a_fir_opt
        a2b2a_opt[:,:,:,6:9] = a2b2a_sec_opt[:,:,:,3:6]
        
        combined_img=im.concat_images(sample_image, a2b_opt)
        combined_img3=im.concat_images(combined_img, a2b2a_opt)

        im.save_images_png(combined_img3,image_path)

    for sample_file in sample_filesB:
        sample_image = [im.load_test_data_nrrd(sample_file, crop_size)]
        sample_image = np.array(sample_image).astype(np.float32)
        image_path = os.path.join(b_save_dir,
                                  '{0}_{1}'.format('BtoA', os.path.basename(sample_file)))

        [b2a_fir_opt, b2a_sec_opt, b2a2b_fir_opt, b2a2b_sec_opt] \
            = sess.run([b2a_fir, b2a_sec, b2a2b_fir, b2a2b_sec], feed_dict={b_real: sample_image})

        # copy 6 dim images back to 9 dim images
        b2a_opt = np.zeros(shape=(1, 256, 256, 9))
        b2a_opt[:, :, :, :6] = b2a_fir_opt
        b2a_opt[:, :, :, 6:9] = b2a_sec_opt[:, :, :, 3:6]

        b2a2b_opt = np.zeros(shape=(1, 256, 256, 9))
        b2a2b_opt[:, :, :, :6] = b2a2b_fir_opt
        b2a2b_opt[:, :, :, 6:9] = b2a2b_sec_opt[:, :, :, 3:6]

        combined_img = im.concat_images(sample_image, b2a_opt)
        combined_img3 = im.concat_images(combined_img, b2a2b_opt)

        im.save_images_png(combined_img3, image_path)
