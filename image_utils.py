"""
Some codes are modified from https://github.com/Newmu/dcgan_code

These functions are all based on [-1.0, 1.0] image
"""

from __future__ import absolute_import, division, print_function
import scipy.misc
import numpy as np
import matplotlib.pyplot as plt
import cv2
import SimpleITK as sitk
from enum import Enum


def load_test_data_nrrd(image_path, fine_size=256, extract_image = 'all'):
    """
    loads a image sequence in nrrd format
	image is resized to 256x256
    """
    img = imreadnrrd(image_path, extract_image)
    img = cv2.resize(img, (fine_size, fine_size))
    img = img/127.5 - 1
    return img

# loads mini-sequences from both sets (A and B)
def load_train_data_nrrd(image_path, load_size=286, fine_size=256, resize_instead_crop=False, extract_image = 'all'):
    img_A = imreadnrrd(image_path[0], extract_image)
    img_B = imreadnrrd(image_path[1], extract_image)

    if not resize_instead_crop:
        h1 = int(np.ceil(np.random.uniform(1e-2, load_size-fine_size)))
        w1 = int(np.ceil(np.random.uniform(1e-2, load_size-fine_size)))
        img_A = img_A[h1:h1+fine_size, w1:w1+fine_size]
        img_B = img_B[h1:h1+fine_size, w1:w1+fine_size]

        if np.random.random() > 0.5:
            img_A = np.fliplr(img_A)
            img_B = np.fliplr(img_B)
    else:
        img_A = cv2.resize(img_A, (fine_size, fine_size))
        img_B = cv2.resize(img_B, (fine_size, fine_size))

    img_A = img_A/127.5 - 1.
    img_B = img_B/127.5 - 1.

    img_AB = np.concatenate((img_A, img_B), axis=2)
    
    return img_AB

# loads mini-sequences from both sets (A and B)
def load_train_data_npy(image_path, load_size=286, fine_size=256, resize_instead_crop=False, extract_image = 'all'):
    img_A = imreadnpy(image_path[0], extract_image)
    img_B = imreadnpy(image_path[1], extract_image)

    if not resize_instead_crop:
        h1 = int(np.ceil(np.random.uniform(1e-2, load_size-fine_size)))
        w1 = int(np.ceil(np.random.uniform(1e-2, load_size-fine_size)))
        img_A = img_A[h1:h1+fine_size, w1:w1+fine_size]
        img_B = img_B[h1:h1+fine_size, w1:w1+fine_size]

        if np.random.random() > 0.5:
            img_A = np.fliplr(img_A)
            img_B = np.fliplr(img_B)
    else:
        img_A = cv2.resize(img_A, (fine_size, fine_size))
        img_B = cv2.resize(img_B, (fine_size, fine_size))

    img_A = img_A/127.5 - 1.
    img_B = img_B/127.5 - 1.

    img_AB = np.concatenate((img_A, img_B), axis=2)
    
    return img_AB

        
def save_images_png(images, image_path, extract_image = 'third'):

    images = (images +1)* 127.5

    path1 = image_path + '_1.png'
    path2 = image_path + '_2.png'
    path3 = image_path + '_3.png'

    if extract_image == 'all':
        cv2.imwrite(path1, images[0,:,:,0:3])
        cv2.imwrite(path2, images[0,:,:,3:6])
        cv2.imwrite(path3, images[0,:,:,6:9])
    elif extract_image == 'first' :
        cv2.imwrite(path1, images[0,:,:,0:3])
    elif extract_image == 'second' :
        cv2.imwrite(path2, images[0, :, :, 3:6])
    elif extract_image == 'third' :
        cv2.imwrite(path3, images[0, :, :, 6:9])

def concat_images(imga, imgb):
    """
    Combines two color image ndarrays side-by-side.
    """
    size = imga.shape
    size1 = imgb.shape
    # (1, x, y, c)
    # width = height  
    total_width = size[2] + size1[2]   
    new_img = np.zeros(shape=(1, size[1], total_width, size[3]))
    new_img[0,:size[1],:size[2]]=imga
    new_img[0,:size1[1],size[2]:total_width]=imgb
    return new_img
    
def imreadnrrd(path, extract_image):
    image = sitk.ReadImage(path)
    npimage = sitk.GetArrayFromImage(image)

    if extract_image == 'first':
        return npimage[:, :, 0:3]
    elif extract_image == 'second':
        return npimage[:, :, 3:6]
    elif extract_image == 'third':
        return npimage[:, :, 6:9]
    else:
        return npimage
          
def imreadnpy(path, extract_image):
    image = np.load(path)
    h = image.shape[1]
    w = image.shape[2]
    offset = int(np.floor((w-h)/2))
    image1 = image[0,:,offset:offset+h,:]
    image2 = image[1,:,offset:offset+h,:]
    image3 = image[2,:,offset:offset+h,:]
    if extract_image == 'first':
        return image1
    elif extract_image == 'second':
        return image2
    elif extract_image == 'third':
        return image3
    else:
        return np.concatenate((image1,image2,image3), axis=2)