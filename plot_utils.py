#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 14:31:34 2018

@author: bmayer
"""
import plotly
import plotly.graph_objs as go
#import os
#import numpy as np

#os.chdir('/home/bmayer/Documents/tempCycleGAN')

'''
import pickle
with open("d_loss_a_summary.txt", "rb") as fp:   # Unpickling
   d_a = pickle.load(fp)
   
with open("d_loss_b_summary.txt", "rb") as fp:   # Unpickling
   d_b = pickle.load(fp)
'''

def plot_losses(loss_traces, labels = None, path = 'loss_plots/loss_plot.html'):
    if len(loss_traces) > 0:
        if labels == None:
            labels = ['Trace ' + str(l) for l in range(len(loss_traces))]
        #x = np.linspace(0, 1, len(loss_traces[0]))
        x = list(range(len(loss_traces[0])))
        data = []
        for loss_trace,label in zip(loss_traces,labels):
            data.append(go.Scatter(x = x, y = loss_trace, name = label, ))
        plotly.offline.plot(data, filename = path)